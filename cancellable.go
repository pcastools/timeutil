// cancellable provides an object that converts timeouts to context cancellation.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package timeutil

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"
)

// cancellable wraps a cancel function, suspending or enabling the ability to call the cancel function.
type cancellable struct {
	cancel      context.CancelFunc
	m           sync.Mutex
	isCancelled bool
	count       int
}

/*
C converts timeouts to cancellation. This simulates the behaviour of other objects with a SetDeadline method, but using a cancellation function. SetDeadline should be called before each call to WithCancel, just as when working with, for example, an os.File, SetDeadline should be called prior to each call to Read. For example, one can use a context and its cancel function:

	ctx, cancel := context.WithCancel(parentContext)
	cancellable := timeutil.NewCancellable(cancel)
	defer cancellable.Stop()
	cancellable.SetDeadline(deadline)
	cancellable.WithCancel(func() error {
		// do something or other
		// checking ctx for cancellation
		// The context ctx will fire if the deadline is exceeded.
	})
*/
type C struct {
	withCancel func(func() error) error
	m          sync.Mutex
	timeoutC   chan<- time.Time
	okC        <-chan bool
	isClosed   bool
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// execFunc executes the given function, recovering from any panics.
func execFunc(f func() error) (err error) {
	// Recover from any panics
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in function: %v", e)
		}
	}()
	// Call the function
	err = f()
	return
}

// timeoutWorker is the background timeout worker. The worker listens for new timeouts down the channel timeoutC, replying down the okC. Closing the timeoutC will cause the worker to exit; in this case the okC channel will be closed. Must be run in its own go-routine.
func timeoutWorker(c *cancellable, timeoutC <-chan time.Time, okC chan<- bool) {
	// Ensure that okC is closed and the cancel function called on exit
	defer func() {
		c.ForceCancel()
		close(okC)
	}()
	// Loop until done
	var timer *time.Timer
	for {
		if timer == nil {
			t, ok := <-timeoutC
			if !ok {
				return
			}
			c.Reset()
			if t.IsZero() {
				// Do nothing
			} else if d := time.Until(t); d <= 0 {
				c.Cancel()
			} else {
				timer = time.NewTimer(d)
			}
			okC <- true
		} else {
			select {
			case t, ok := <-timeoutC:
				if !timer.Stop() {
					<-timer.C
				}
				if !ok {
					return
				}
				c.Reset()
				if t.IsZero() {
					timer = nil
				} else if d := time.Until(t); d <= 0 {
					c.Cancel()
					timer = nil
				} else {
					timer.Reset(d)
				}
				okC <- true
			case <-timer.C:
				c.Cancel()
				timer = nil
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////
// cancellable functions
/////////////////////////////////////////////////////////////////////////

// newCancellable wraps the given cancel function.
func newCancellable(cancel context.CancelFunc) *cancellable {
	return &cancellable{
		cancel: cancel,
	}
}

// ForceCancel calls the wrapped cancel function.
func (c *cancellable) ForceCancel() {
	if c.cancel != nil {
		c.cancel()
	}
}

// Cancel calls the wrapped cancel function only if WithCancel is being called. Otherwise the cancel function will be called the next time WithCancel is called, unless Reset is called first.
func (c *cancellable) Cancel() {
	c.m.Lock()
	defer c.m.Unlock()
	if c.count == 0 {
		c.isCancelled = true
	} else {
		if c.cancel != nil {
			c.cancel()
		}
		c.isCancelled = false
	}
}

// Reset prevents the cancel function being called the next time WithCancel is called.
func (c *cancellable) Reset() {
	c.m.Lock()
	defer c.m.Unlock()
	c.isCancelled = false
}

// WithCancel permits the wrapped cancel function to be called whilst executing f.
func (c *cancellable) WithCancel(f func() error) error {
	// Increment the number of cancellable tasks running
	c.m.Lock()
	c.count++
	if c.isCancelled {
		if c.cancel != nil {
			c.cancel()
		}
		c.isCancelled = false
	}
	c.m.Unlock()
	// Run the user function
	err := execFunc(f)
	// Record that this cancellable task has finished
	c.m.Lock()
	c.count--
	c.m.Unlock()
	// Return any error
	return err
}

/////////////////////////////////////////////////////////////////////////
// C functions
/////////////////////////////////////////////////////////////////////////

// NewCancellable creates a new C, starting a background worker handling timeouts. The user must call Stop, otherwise resources will leak. See the documentation for C for further discussion and example usage.
func NewCancellable(cancel context.CancelFunc) *C {
	// Create the cancellable and communication channels
	c := newCancellable(cancel)
	timeoutC, okC := make(chan time.Time), make(chan bool)
	// Start the worker in a new go-routine and return
	go timeoutWorker(c, timeoutC, okC)
	return &C{
		timeoutC:   timeoutC,
		okC:        okC,
		withCancel: c.WithCancel,
	}
}

// Stop stops the background timeout worker.
func (c *C) Stop() {
	// Sanity check
	if c != nil {
		// Trigger a shutdown of the background worker
		c.m.Lock()
		if !c.isClosed {
			c.isClosed = true
			close(c.timeoutC)
			<-c.okC
		}
		c.m.Unlock()
	}
}

// WithCancel permits the wrapped cancel function to be called whilst executing f.
func (c *C) WithCancel(f func() error) error {
	if c == nil {
		return errors.New("uninitialised C")
	}
	return c.withCancel(f)
}

// SetDeadline sets the deadline for future WithCancel calls and any currently-blocked WithCancel calls. A zero value for t means WithCancel will not time out.
func (c *C) SetDeadline(t time.Time) error {
	// Sanity check
	if c == nil {
		return errors.New("uninitialised C")
	}
	// Update the timeout
	c.m.Lock()
	if c.isClosed {
		c.m.Unlock()
		return errors.New("closed C")
	}
	c.timeoutC <- t
	<-c.okC
	c.m.Unlock()
	return nil
}
