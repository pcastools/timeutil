// Cancellable_test provides tests for cancellable objects.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package timeutil

import (
	"context"
	"testing"
	"time"
)

func TestStop(t *testing.T) {
	// Start a timeout worker
	cancelled := false
	c := NewCancellable(func() { cancelled = true })
	// Stop it and check that the cancel function was called
	c.Stop()
	if !cancelled {
		t.Fatal("cancel function wasn't called")
	}
	// Attempting to set a deadline after Stop should error
	if err := c.SetDeadline(time.Now()); err == nil {
		t.Fatal("calling SetDeadline after Stop should generate an error")
	}
}

func TestImmediateTimeout1(t *testing.T) {
	// Create the context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Start a timeout worker
	c := NewCancellable(cancel)
	defer c.Stop()
	// Set an immediate timeout
	if err := c.SetDeadline(time.Now()); err != nil {
		t.Fatalf("error setting deadline: %v", err)
	}
	// Check that the cancel function was called
	failed := false
	c.WithCancel(func() error {
		select {
		case <-ctx.Done():
		default:
			failed = true
		}
		return nil
	})
	// Did this work?
	if failed {
		t.Fatal("timeout failed to fire")
	}
}

func TestImmediateTimeout2(t *testing.T) {
	// Create the context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Start a timeout worker
	c := NewCancellable(cancel)
	defer c.Stop()
	// Set a timeout in the future
	if err := c.SetDeadline(time.Now().Add(time.Minute)); err != nil {
		t.Fatalf("error setting deadline: %v", err)
	}
	// Set an immediate timeout
	if err := c.SetDeadline(time.Now()); err != nil {
		t.Fatalf("error setting deadline: %v", err)
	}
	// Check that the cancel function was called
	failed := false
	c.WithCancel(func() error {
		select {
		case <-ctx.Done():
		default:
			failed = true
		}
		return nil
	})
	// Did this work?
	if failed {
		t.Fatal("timeout failed to fire")
	}
}

func TestDelayedTimeout(t *testing.T) {
	// Create the context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Start a timeout worker
	c := NewCancellable(cancel)
	defer c.Stop()
	// Set a timeout and let it expire
	if err := c.SetDeadline(time.Now().Add(5 * time.Millisecond)); err != nil {
		t.Fatalf("error setting deadline: %v", err)
	}
	time.Sleep(10 * time.Millisecond)
	// Now run a function
	failed := false
	c.WithCancel(func() error {
		select {
		case <-ctx.Done():
		default:
			failed = true
		}
		return nil
	})
	// Did this work?
	if failed {
		t.Fatal("timeout failed to fire")
	}
}

func TestTimeout(t *testing.T) {
	// Create the context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Start a timeout worker
	c := NewCancellable(cancel)
	defer c.Stop()
	// Set a timeout
	if err := c.SetDeadline(time.Now().Add(10 * time.Millisecond)); err != nil {
		t.Fatalf("error setting deadline: %v", err)
	}
	// Wait for the timeout
	failed := false
	c.WithCancel(func() error {
		t := time.NewTimer(100 * time.Millisecond)
		select {
		case <-ctx.Done():
			t.Stop()
		case <-t.C:
			failed = true
		}
		return nil
	})
	// Did this work?
	if failed {
		t.Fatal("timeout failed to fire after 100 milliseconds")
	}
}

func TestNoTimeout(t *testing.T) {
	// Start a timeout worker
	cancelled := false
	c := NewCancellable(func() { cancelled = true })
	defer c.Stop()
	// Set a timeout and wait
	if err := c.SetDeadline(time.Now().Add(time.Millisecond)); err != nil {
		t.Fatalf("error setting deadline: %v", err)
	}
	time.Sleep(10 * time.Millisecond)
	// Check that the cancel function wasn't called
	if cancelled {
		t.Fatal("cancel function called when it shouldn't have been")
	}
}

func TestResetTimeout(t *testing.T) {
	// Create the context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Start a timeout worker
	c := NewCancellable(cancel)
	defer c.Stop()
	// Set a timeout and wait for it to expire
	if err := c.SetDeadline(time.Now().Add(time.Millisecond)); err != nil {
		t.Fatalf("error setting deadline: %v", err)
	}
	time.Sleep(10 * time.Millisecond)
	// The context should not have fired
	select {
	case <-ctx.Done():
		t.Fatal("cancel function called when it shouldn't have been")
	default:
	}
	// Set a fresh timeout
	if err := c.SetDeadline(time.Now().Add(time.Minute)); err != nil {
		t.Fatalf("error setting deadline: %v", err)
	}
	// We should be able to successfully run our function
	failed := false
	c.WithCancel(func() error {
		t := time.NewTimer(3 * time.Millisecond)
		select {
		case <-ctx.Done():
			failed = true
			t.Stop()
		case <-t.C:
		}
		return nil
	})
	// Did this work?
	if failed {
		t.Fatal("timeout fired unexpectedly")
	}
	// Set a fresh timeout
	if err := c.SetDeadline(time.Now().Add(3 * time.Millisecond)); err != nil {
		t.Fatalf("error setting deadline: %v", err)
	}
	// Wait for the timeout to fire
	c.WithCancel(func() error {
		t := time.NewTimer(100 * time.Millisecond)
		select {
		case <-ctx.Done():
			t.Stop()
		case <-t.C:
			failed = true
		}
		return nil
	})
	// Did this work?
	if failed {
		t.Fatal("timeout failed to fire after 100 milliseconds")
	}
}

func TestNilValue(t *testing.T) {
	var c *C
	// WithCancel should return an error
	if err := c.WithCancel(func() error { return nil }); err == nil {
		t.Fatal("calling WithCancel on a nil C failed to return an error")
	}
	// SetDeadline should return an error
	if err := c.SetDeadline(time.Now()); err == nil {
		t.Fatal("calling SetDeadline on a nil C failed to return an error")
	}
}
